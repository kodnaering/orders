const express = require('express')
const path = require('path')
const cors = require('cors')
const parse = require('csv-parse')
const fs = require('fs')
const highland = require('highland')

const app = express()
app.use(cors())

app.get("/", (req, res) => {
	const input = highland(fs.createReadStream('data/assignment-orders.csv'))
	const parser = parse({
		delimiter: ';',
		columns: true,
		auto_parse: true,
	})
	const page = parseInt(req.query.page || 0, 10)
	const size = parseInt(req.query.size || 10, 10)
	let current = 0
	const stream = input
		.pipe(parser)
		.pipe(highland())
	stream
		.consume((err, row, push, next) => {
			if (err) {
				console.error('consumer error', err)
				push(null, highland.nil)
				return
			}
			if (row === highland.nil) {
				push(null, highland.nil)
				return
			}
			if (current >= page * size + size) {
				push(null, highland.nil)
				return
			} else if (current >= page * size) {
				push(null, row)
			}
			current = current + 1
			next()
		})
		.toArray(output => {
			if (res.headersSent) {
				return
			}
			if (output.length === 0) {
				res.status(404)
				res.send('Page not found')
			} else {
				res.send(output)
			}
		})
	stream
		.on('error', err => {
			console.error('stream error', err)
			res.status(500)
			res.send('stream error')
		})
	parser
		.on('error', err => {
			console.error('parser error', err)
			res.status(500)
			res.send('parser error')
		})
	input
		.on('error', err => {
			console.error('input error', err)
			res.status(500)
			res.send('input error')
		})
})

app.listen(4300, () => {
	console.info("Data server started!")
})
